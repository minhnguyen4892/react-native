import { stringify } from 'query-string';
import { decamelizeKeys } from 'humps';

export const queryParams = params => {
    const tmpParams = decamelizeKeys(params);
    return stringify(tmpParams, { encode: true, arrayFormat: 'bracket' });
};

export const passUrlQueryParams = (url, params) => {
    if (!params) return url;
    return url + (url.indexOf('?') === -1 ? '?' : '&') + queryParams(params);
};
