import axios from 'axios';
import { camelizeKeys } from 'humps';

export const CALL_API = 'CALL_API';

const callApi = (endpoint, method, params, isFile) => {
    if (endpoint && (endpoint.indexOf('https://') === -1 && endpoint.indexOf('http://') === -1)) {
        endpoint = `https://${endpoint}`;
    }
    let config = {
        method: method,
        url: endpoint,
        data: params,
        headers: {
            'X-Requested-By': 'frontend'
        }
    };
    if (method !== 'GET') {
        if (isFile) {
            config.headers = {
                ...config.headers,
                'Content-Type': 'multipart/form-data'
            };
            config.body = params;
        } else {
            config.body = JSON.stringify(params);
        }
    }

    return axios(config).then(response => response, error => Promise.resolve(error.response));
};

export const customMiddleWare = store => next => action => {
    if (action) {
        const callAPI = action[CALL_API];
        if (typeof callAPI === 'undefined') {
            return next(action);
        }

        let { endpoint, params, file } = callAPI;
        const { method, types } = callAPI;
        if (typeof endpoint === 'function') {
            endpoint = endpoint(store.getState());
        }

        if (typeof method !== 'string') {
            throw new Error('Specify a string method for calling.');
        }

        if (typeof endpoint !== 'string') {
            throw new Error('Specify a string endpoint URL.');
        }

        if (!Array.isArray(types) || types.length !== 3) {
            throw new Error('Expected an array of three action types.');
        }

        if (!types.every(type => typeof type === 'string')) {
            throw new Error('Expected action types to be strings.');
        }

        const actionWith = data => {
            const finalAction = { ...action, ...data };
            delete finalAction[CALL_API];
            return finalAction;
        };

        const [requestType, successType, failureType] = types;

        next(actionWith({ type: requestType }));

        return callApi(endpoint, method, params || {}, file).then(
            response => {
                const responseToJson = JSON.parse(JSON.stringify(response));
                console.log('response test 111', responseToJson);
                return next(
                    actionWith({
                        payload: camelizeKeys(responseToJson.data),
                        type: successType
                    })
                );
            },
            error => {
                return next(
                    actionWith({
                        type: failureType,
                        status: (error && error.status) || 500,
                        payload: camelizeKeys(error.data) || {}
                    })
                );
            }
        );
    }
};

// export const callAPI = ({
//                             url,
//                             method,
//                             body
//                         }) => {
//     // console.log('url-method-body', url, method, body)
//     let config = {}
//     if (method === "POST" || method === "PATCH") {
//         config.method = method
//         config.headers = {
//             Accept: 'application/json',
//             'Content-Type': 'application/json'
//         }
//         config.body = JSON.stringify(body)
//     }
//     if (method === "GET") {
//         config.method = method
//     }
//     return fetch(
//         url,
//         config
//     )
//         .then(response => response.json())
// };