import HomeIconWithBadge from '../HomeIconWithBadge';
import Ionicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import {ROUTES} from "../../constants";

const getTabBarIcon = (navigation, focused, tintColor) => {
    const { routeName } = navigation.state;
    let IconComponent = Ionicons;
    let iconName;
    switch (routeName) {
        case ROUTES.DASHBOARD:
            iconName = `ios-information-circle${focused ? '' : '-outline'}`;
            return <HomeIconWithBadge
                name={iconName}
                size={25}
                color={tintColor}
                badgeCount={3}
            />;
        case ROUTES.SETTING:
            iconName = `ios-options`;
            return <IconComponent
                name={iconName}
                size={25}
                color={tintColor}
            />;
        case ROUTES.MAP:
            iconName = `ios-map`;
            return <IconComponent
                name={iconName}
                size={25}
                color={tintColor}
            />;

    }
};

export {
    getTabBarIcon
};