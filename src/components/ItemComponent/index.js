import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { List, ListItem, SearchBar } from 'react-native-elements';

class ItemComponent extends PureComponent {
    state = {
        loading: false,
        refreshing: false
    };

    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large"/>
            </View>
        )
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: '86%',
                    marginLeft: '14%',
                    backgroundColor: '#CED0CE'
                }}
            />
        )
    };

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round />;
    };

    handleRefresh = () => {
        this.setState({ refreshing: true });
    };

    render() {
        const { items, openDetailPage } = this.props;
        return (
            <FlatList
                keyExtractor={({ id } ) => id}
                data={items}
                renderItem={({ item }) => (
                    <ListItem
                        roundAvatar
                        title={`${item.title}`}
                        subtitle={item.description}
                        leftAvatar={{ source: { uri: item.urlImage } }}
                        rightTitle={item.time}
                        chevron={true}
                        badge={{ value: 3, textStyle: { color: 'white' } }}
                        containerStyle={{ borderTopWidth: 0 }}
                        onPress={openDetailPage}
                    />
                )}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                ItemSeparatorComponent={this.renderSeparator}
                refreshing={this.state.refreshing}
                onRefresh={this.handleRefresh}
            />
        )
    }
}

{/*<TouchableOpacity onPress={openDetailPage}>*/}
{/*    <View style={[styles.item, styles.directionRow]} key={item.key}>*/}
{/*        <View style={styles.directionRow}>*/}
{/*            <View style={styles.imageWrapper}>*/}
{/*                <Image*/}
{/*                    source={{*/}
{/*                        uri: item.urlImage*/}
{/*                    }}*/}
{/*                    style={styles.image}*/}
{/*                />*/}

{/*            </View>*/}
{/*            <View style={styles.content}>*/}
{/*                <Text style={styles.title}>{item.title}</Text>*/}
{/*                <Text>{item.description}</Text>*/}
{/*            </View>*/}
{/*        </View>*/}
{/*        <View>*/}
{/*            <Text>{item.time}</Text>*/}
{/*        </View>*/}
{/*    </View>*/}

{/*</TouchableOpacity>*/}

const styles = StyleSheet.create({
    directionRow: {
        flexDirection: 'row'
    },
    item: {
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#888'
    },
    imageWrapper: {
        borderBottomColor: '#fff'
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 40
    },
    content: {
        padding: 10
    },
    title: {
        marginBottom: 10
    }
});

export default ItemComponent;