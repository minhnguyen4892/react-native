import { CALL_API } from "../utils";
import {passUrlQueryParams} from "../utils";
export const FETCH_TODO_REQUEST = 'fetch_todo_request';
export const FETCH_TODO_SUCCESS = 'fetch_todo_success';
export const FETCH_TODO_FAILURE = 'fetch_todo_failure';

export const CREATE_TODO_REQUEST = 'create_todo_request';
export const CREATE_TODO_SUCCESS = 'create_todo_success';
export const CREATE_TODO_FAILURE = 'create_todo_failure';

export const UPDATE_TODO_REQUEST = 'update_todo_request';
export const UPDATE_TODO_SUCCESS = 'update_todo_success';
export const UPDATE_TODO_FAILURE = 'update_todo_failure';

export const DELETE_TODO_REQUEST = 'detete_todo_request';
export const DELETE_TODO_SUCCESS = 'delete_todo_success';
export const DELETE_TODO_FAILURE = 'delete_todo_failure';
export const APP_URL = 'https://jsonplaceholder.typicode.com';
export const POST_URL = '/posts';

export const fetchTodos = params => dispatch => {
    const fullPostUrl = `${APP_URL}${passUrlQueryParams(POST_URL, params)}`;
    return dispatch({
        [CALL_API]: {
            types: [
                FETCH_TODO_REQUEST,
                FETCH_TODO_SUCCESS,
                FETCH_TODO_FAILURE
            ],
            method: 'GET',
            endpoint: fullPostUrl
        }
    })
};

export const createTodo = params => dispatch => {
    const fullCreatePostUrl = `${APP_URL}${POST_URL}`;
    return dispatch({
        [CALL_API]: {
            types: [
                CREATE_TODO_REQUEST,
                CREATE_TODO_SUCCESS,
                CREATE_TODO_FAILURE
            ],
            method: 'POST',
            endpoint: fullCreatePostUrl,
            params: params
        }
    });
};

export const updateTodo = params => dispatch => {
    const fullUpdatePostUrl = `${APP_URL}${POST_URL}/${params.id}`;
    return dispatch({
        [CALL_API]: {
            types: [
                UPDATE_TODO_REQUEST,
                UPDATE_TODO_SUCCESS,
                UPDATE_TODO_FAILURE
            ],
            method: 'PATCH',
            endpoint: fullUpdatePostUrl,
            params: params.data
        }
    });
};

export const deleteTodo = params => dispatch => {
    const fullDeleteTodotUrl = `${APP_URL}${POST_URL}/${params.id}`;
    return dispatch({
        [CALL_API]: {
            types: [
                DELETE_TODO_REQUEST,
                DELETE_TODO_SUCCESS,
                DELETE_TODO_FAILURE
            ],
            method: 'POST',
            endpoint: fullDeleteTodotUrl
        }
    });
};