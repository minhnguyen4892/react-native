import {AsyncStorage} from "react-native";


const getUserInfo = async () => {
    let user = null;
    try {
        user =  await AsyncStorage.getItem('user');
        console.log('debug home page 1', user);
        return user;
    } catch (e) {
        console.log('Get user info error', e)
    }
}

const removeUserInfo = async () => {
    let user = null;
    try {
        user =  await AsyncStorage.removeItem('user');
        console.log('remove user', user)
    } catch (e) {
        console.log('Remove user info error', e)
    }
    return user
}

export {
    getUserInfo,
    removeUserInfo
}