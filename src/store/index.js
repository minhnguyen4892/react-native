import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '../reducers';
import {customMiddleWare} from '../utils';

const configureStore = () => {
    let store;
    if (process.env.NODE_ENV !== 'production') {
        store = createStore(
            rootReducer,
            composeWithDevTools(applyMiddleware(thunk, customMiddleWare))
        );
        if (module.hot) {
            module.hot.accept('./reducers', () => {
                store.replaceReducer(rootReducer);
            });
        }
    } else {
        store = createStore(rootReducer, applyMiddleware(thunk, customMiddleWare));
    }
    return store;
};

export default configureStore;