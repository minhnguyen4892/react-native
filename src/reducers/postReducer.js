import {
    FETCH_TODO_SUCCESS
} from "../actions";

const initialState = {
    posts: [],
    isLoading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}