import React from "react";
import {Text, TouchableOpacity, View, ActivityIndicator, StyleSheet, FlatList, Modal, TouchableHighlight, SafeAreaView} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Image, ListItem, Input } from 'react-native-elements';
import Overlay, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';
import {getUserInfo, removeUserInfo} from "../../actions";
import { USER_PROFILE, USER_DATA } from '../../constants/userConstants';


export default class ProfileScreen extends React.Component {
    state = {
        modalVisible: false,
        selectedData: {}
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    };


    renderModal = () => {
        let { selectedData } = this.state;
        return (
            <View style={{marginTop: 22}}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    presentationStyle='formSheet'
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={{marginTop: 22}}>
                        <View>
                            <Input
                                placeholder='Username'
                                leftIcon={
                                    <Icon
                                        name={selectedData.icon}
                                        size={24}
                                        color='black'
                                    />
                                }
                                value={selectedData.subtitle}
                                onChangeText={(text) => {
                                    const data =  { subtitle: text };
                                    this.setState({
                                        ...selectedData,
                                        ...data
                                    },  ()=> {

                                        console.log('selectedData', this.state.selectedData);
                                    })
                                }}
                            />

                            <TouchableHighlight
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text>Cancel</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>

                <TouchableHighlight
                    onPress={() => {
                        this.setModalVisible(true);
                    }}>
                    <Text>Show Modal</Text>
                </TouchableHighlight>
            </View>
        );
    };

    renderPopup = () => {
        let { selectedData } = this.state;
       return (
           <View>
           <Overlay
               visible={this.state.modalVisible}
               width={0.8}
               animationDuration={150}
               useNativeDriver={true}
               footer={
                   <DialogFooter>
                       <DialogButton
                           text="CANCEL"
                           onPress={() => {
                               this.setModalVisible(!this.state.modalVisible);
                           }}
                       />
                       <DialogButton
                           text="OK"
                           onPress={() => {
                               this.setModalVisible(!this.state.modalVisible);
                           }}
                       />
                   </DialogFooter>
               }
           >
               <DialogContent>
                   <Input
                       placeholder='Username'
                       leftIcon={
                           <Icon
                               name={selectedData.icon}
                               size={24}
                               color='black'
                           />
                       }
                       multiline
                       textAlignVertical='top'
                       value={selectedData.subtitle}
                       onChangeText={(text) => {
                           const data =  { subtitle: text };
                           this.setState({
                               selectedData: {
                                   ...selectedData,
                                   ...data
                               }
                           },  ()=> {
                               console.log('selectedData-data', this.state.selectedData, data);
                           })
                       }}
                   />

               </DialogContent>
           </Overlay>
       </View>
       )
    };

    render() {
        if(!getUserInfo()) {
            this.props.navigation.navigate('Login')
        }
        return (
            <View style={styles.wrapper}>
                <View style={styles.image}>
                    <Image
                        source={{ uri: USER_PROFILE.urlImage }}
                        style={{
                            width: 150,
                            height: 150,
                            borderRadius: 75
                        }}
                        PlaceholderContent={<ActivityIndicator />}
                    />
                </View>
                <View>
                    <FlatList
                        keyExtractor={({ id } ) => id}
                        data={USER_DATA}
                        renderItem={({ item }) => (
                            <ListItem
                                title={item.name}
                                subtitle={item.subtitle}
                                leftIcon={() => (
                                    <Icon
                                        name={item.leftIcon} />
                                )}
                                rightIcon={() => (
                                    <MaterialIcon
                                        name={item.rightIcon} />
                                )}
                                containerStyle={{ borderTopWidth: 0 }}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible)
                                    this.setState({ selectedData: {...item} })
                                }}
                            />
                        )}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => {
                        removeUserInfo();
                        this.props.navigation.navigate('Login');
                    }}>
                    <Text>Log out</Text>
                </TouchableOpacity>
                {this.renderPopup()}
            </View>
        );
    }
};

const styles = StyleSheet.create({
    wrapper: {
       flex: 1,
       justifyContent: 'flex-start',
       padding: 10
    },
    container: {
        flex: 1,
        width: 200,
        height: 200
    },
    image: {
       alignItems: 'center'
    }
});