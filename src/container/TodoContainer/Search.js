import React from 'react'
import { View, TextInput } from 'react-native'

export default SearchComponent = (props) => {
    return (
        <View>
            <TextInput
                editable={true}
                value={props.searchValue}
                onChangeText={props.onSearchChange}
                onSubmitEditing={e => console.log('debug',e)}
                returnKeyType='done'
                placeholder='Search something'
            />
        </View>
    )
}