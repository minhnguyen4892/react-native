import React from 'react'
import {View, StyleSheet, ActivityIndicator} from 'react-native'
import AddTodo from "./AddTodo"
import DetailItem from "./ItemDetail"
import SearchComponent from "./Search"
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {
    fetchTodos,
    createTodo,
    updateTodo,
    deleteTodo,
    getUserInfo
} from "../../actions";

class TodoComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            inputValue: '',
            selectedPost: {},
            isEdit: false,
            posts: [],
            searchValue: '',
            matchPosts: []
        }
    }

    componentDidMount(): void {
        const { fetchTodos } = this.props;
        fetchTodos({
            limit: 5
        })
            // .then(responseData => {
            //     console.log('get posts data successfully', responseData);
            //     this.setState(prevState => ({
            //         ...prevState,
            //         loading: false,
            //         posts: responseData
            //     }), () => console.log('get posts data successfully'))
            // })
            // .catch(error => console.log('get posts data error', error))
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.postData !== prevState.postData) {
            console.log('postData', nextProps.postData);
            return {
                posts: nextProps.postData.posts
            }
        }
        return null;
    }

    handleAddPostItem = () => {
        const { createTodo } = this.props;
        createTodo({
            title: this.state.inputValue,
            body: this.state.inputValue,
            userId: 1
        })
            .then(response => {
                this.setState(prevState => ({
                    ...prevState,
                    posts: [...this.state.posts, response]
                }))
            })
            .catch((error) => console.log('create post error', error))
    };

    handleEditPostItem = () => {
        const { updateTodo } = this.props;
        updateTodo({
            id: this.state.selectedPost.id,
            data: {
                title: this.state.inputValue
            }
        })
            .then(response => {
                const postIndex = [...this.state.posts].findIndex(post => post.id === response.id)
                if (postIndex > -1) {
                    const newPostArray = [...this.state.posts]
                    newPostArray[postIndex].title = response.title
                    this.setState(prevState => ({
                        ...prevState,
                        posts: newPostArray
                    }))
                }
            })
            .catch(error => console.log('error edit post item', error))
    };

    onEditPostItemEvent = (item) => {
        this.setState(prevState => ({
            ...prevState,
            selectedPost: item,
            inputValue: item.title,
            isEdit: true
        }), () => console.log('selectedPost 111 data', this.state.selectedPost))
    };

    handleChangeTextTodo = (text) => {
        this.setState(prevState => ({
                ...prevState,
                inputValue: text
            }
        ))
    };

    handleSeachValueChange = async (text) => {
        await this.setState(prevState => ({
            ...prevState,
            searchValue: text
        }))
    };
    handleKeyPress = () => {
        console.log('debug 111', this.state.searchValue)
        const regex = new RegExp(this.state.searchValue, 'i')
        const matchPosts = [...this.state.posts].filter(post => post.title && post.title.match(regex))
        this.setState({posts: matchPosts})
        // switch (event.key) {
        //     case 'Enter':
        //         // _.debounce( this.handleSearch, 500)
        //         this.handleSearch()
        //         break
        //     default:
        //         return
        // }
    };

    handleSearch() {
        console.log('debug search value', this.state.searchValue)
        const regex = new RegExp(this.state.searchValue, 'i')
        const matchPosts = [...this.state.posts].filter(post => post.title && post.title.match(regex))
        this.setState({posts: matchPosts})
    }

    handleDeletePostItem = (id) => {
        const { deleteTodo } = this.props;
        deleteTodo({
            id
        })
            .then(response => {
                const filterdPosts = [...this.state.posts].filter(post => post.id !== id)
                this.setState(prevState => ({
                    ...prevState,
                    posts: [...filterdPosts]
                }))
            })
            .catch(error => console.log('Delete post error', error))
    };

    render() {
        if (!getUserInfo()) {
            this.props.navigation.navigate("Login")
        } else {
            if (this.state.loading) {
                return (
                    <View style={styles.wrapper}>
                        <ActivityIndicator/>
                    </View>
                )
            }
            return (
                <View style={styles.wrapper}>
                    <SearchComponent
                        seachValue={this.state.searchValue}
                        onSearchChange={this.handleSeachValueChange}
                        onSearchKeyPress={this.handleKeyPress}
                    />
                    <DetailItem
                        items={this.state.posts}
                        onEditPostItem={this.handleDeletePostItem}
                        onDeletePostItem={this.handleDeletePostItem}
                        onEditPostItemEvent={this.onEditPostItemEvent}
                    />
                    <AddTodo
                        onAddPostItem={this.handleAddPostItem}
                        handleEditPostItem={this.handleEditPostItem}
                        onChange={this.handleChangeTextTodo}
                        inputValue={this.state.inputValue}
                        isEdit={this.state.isEdit}
                    />
                </View>
            )
        }
    }
}
const mapStateFromProps = (state) => {
    return {
        postData: state.postData
    }
};

const mapDispatchFromProps = (dispatch)=> ({
    dispatch,
    ...bindActionCreators(
        {
            fetchTodos,
            createTodo,
            updateTodo,
            deleteTodo
        },
        dispatch
    )
});

export default connect(mapStateFromProps, mapDispatchFromProps)(TodoComponent);

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    }
});

