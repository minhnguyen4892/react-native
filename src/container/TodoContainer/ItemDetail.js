import React from 'react'
import { FlatList, View, Text, TouchableOpacity, StyleSheet } from 'react-native'

export default ItemDetailComponent = (props) => {
    return (
        <FlatList
            keyExtractor={({id}, index) => id}
            data={props.items}
            renderItem={({item}) => (
                <View style={styles.itemDetail}>
                    <Text>{item.id} - {item.title}</Text>
                    <TouchableOpacity onPress={() => props.onEditPostItemEvent(item)}>
                        <Text>EDIT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.onDeletePostItem(item.id)}>
                        <Text>DELETE</Text>
                    </TouchableOpacity>
                </View>
            )} />
    )
}

const styles = StyleSheet.create({
    itemDetail: {
        flexDirection: 'row',
    }
})