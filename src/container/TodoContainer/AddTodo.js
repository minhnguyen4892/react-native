import React from 'react'
import {Text, TextInput, TouchableOpacity, View, StyleSheet} from "react-native";

export default AddToDoComponent = (props) => {
    return (
        <View style={styles.addItemWapper}>
            <TextInput
                style={styles.height}
                placeholder="Enter title post item"
                multiline={true}
                numberOfLines={4}
                autoFocus={true}
                editable={true}
                value={props.inputValue}
                onChangeText={props.onChange}
            />
            {!props.isEdit && <TouchableOpacity onPress={props.onAddPostItem}>
                <Text>ADD</Text>
            </TouchableOpacity>
            }
            {props.isEdit && <TouchableOpacity onPress={props.handleEditPostItem}>
                <Text>UPDATE</Text>
            </TouchableOpacity>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    addItemWapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    addItem: {
        height: 40
    }
})