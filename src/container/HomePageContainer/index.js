import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ScrollableTabView, { ScrollableTabBar }  from 'react-native-scrollable-tab-view';
import {getUserInfo} from "../../actions";
import { MENUES, MENU_KEY } from "../../constants";
import ItemComponent from '../../components/ItemComponent';


class HomeScreen extends React.Component {

    renderDefaultItem = (item) => {
        return (
            <View tabLabel={item.value} key={item.key} style={styles.wrapper}>
                <Text>{item.value}</Text>
            </View>
        )
    };

    render() {
        const { navigation } = this.props;
        if(!getUserInfo()) {
            navigation.navigate('Login');
        }
        return (
            <ScrollableTabView
                style={{marginTop: 20, }}
                initialPage={1}
                renderTabBar={() => <ScrollableTabBar />}
            >
                {
                    MENUES.map(item =>  {
                        if (item.key === MENU_KEY.SHOPPING) {
                            return (
                                <View tabLabel={item.key} key={item.key}>
                                    <ItemComponent items={item.value} openDetailPage={() => navigation.navigate('DetailPage')}/>
                                </View>
                            )
                        } else {
                            return this.renderDefaultItem(item);
                        }
                    })

                }
            </ScrollableTabView>
        );
    }
}

export default HomeScreen;

const styles = StyleSheet.create({
   wrapper: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center'
   }
});
