import React, { PureComponent } from 'react';
import { View, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ListItem} from "react-native-elements";
import { USER_PROFILE } from '../../constants/userConstants';
import {SETTING_DATA} from "../../constants";

export default class SettingComponent extends PureComponent {
    render () {
        const { navigation } = this.props;
        return (
            <View>
                <ListItem
                    roundAvatar
                    title={`${USER_PROFILE.userName}`}
                    subtitle={USER_PROFILE.phone}
                    leftAvatar={{ source: { uri: USER_PROFILE.urlImage } }}
                    chevron={true}
                    containerStyle={{ borderBottomWidth: 0.25 }}
                    onPress={() => navigation.navigate('Profile')}
                />
                <View style={{backgroundColor: 'pink'}}>
                    <FlatList
                        keyExtractor={({ id } ) => id}
                        data={SETTING_DATA}
                        renderItem={({item}) => (
                            <ListItem
                                title={item.name}
                                subtitle={item.description}
                                leftIcon={() => (
                                    <Icon
                                        name={item.icon}
                                    />
                                )}
                            />
                        )}
                    >
                    </FlatList>
                </View>
            </View>
        )
    }
}