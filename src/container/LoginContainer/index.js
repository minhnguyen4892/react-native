import React from 'react'
import { View, TextInput, TouchableOpacity, Text, StyleSheet, AsyncStorage, Alert } from 'react-native'

export default class LoginComponent extends React.PureComponent {
    constructor(props, context) {
        super(props, context);
        this.state = {
            username: '',
            password: ''
        }
    }

    handleLogin = async () => {
        const { username, password } = this.state
        if (!username || !password) {
            Alert.alert('Please enter username and password')
        } else {
            try {
                await AsyncStorage.setItem('user', JSON.stringify({
                    username,
                    password
                }));
                this.props.navigation.navigate('Main');
            } catch (e) {
                console.log('Storage username, password error', e)
            }
        }

    }

    render() {
        return (
            <View style={[styles.wrapper, styles.center]}>
                <TextInput
                    style={[styles.item, styles.commonLayout]}
                    value={this.state.username}
                    placeholder='Enter username'
                    autoCompleteType='username'
                    onChangeText={text => this.setState(prevState => ({
                        ...prevState,
                        username: text
                    }))}
                />
                <TextInput
                    style={[styles.item, styles.commonLayout]}
                    value={this.state.password}
                    placeholder='Enter password'
                    autoCompleteType='password'
                    onChangeText={text => this.setState(prevState => ({
                        ...prevState,
                        password: text
                    }))}
                />
                <TouchableOpacity
                    onPress={this.handleLogin}
                    style={[styles.submitBtn, styles.center, styles.commonLayout]}
                >
                    <Text style={styles.contentText}>Submit</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    item: {
        borderColor: '#ccc',
        marginBottom: 40,
        padding: 10,
    },
    commonLayout: {
        height: 40,
        width: 200,
        borderWidth: 1,
        borderRadius: 5,
    },
    submitBtn: {
        backgroundColor: '#12799f',
        borderColor: '#12799f',
    },
    contentText: {
        color: '#ffffff'
    }
})