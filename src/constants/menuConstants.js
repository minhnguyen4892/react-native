const MENUES = [
    { key: 'RESTAURANT', value: 'RESTAURANT' },
    { key: 'SHOPPING', value: [{
            id: 1,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },{
            id: 2,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },{
            id: 3,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },{
            id: 4,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },{
            id: 5,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },{
            id: 6,
            urlImage: 'https://facebook.github.io/react/logo-og.png',
            title: 'User 1',
            description: 'Content',
            time: '3 days',
            icon: 'chevron-right'
        },
        ]
    },
    { key: 'GYM', value: 'GYM' },
    { key: 'MEETING ROOM', value: 'MEETING ROOM'},
    { key: 'HOTEL', value: 'HOTEL'},
    { key: 'ENTERTAINMENT', value: 'ENTERTAINMENT'}
];

const MENU_KEY = {
    RESTAURANT: 'RESTAURANT',
    SHOPPING: 'SHOPPING',
    GYM: 'GYM',
    MEETING_ROOM: 'MEETING ROOM',
    HOTEL: 'HOTEL',
    ENTERTAINMENT: 'ENTERTAINMENT'
};

export {
    MENU_KEY,
    MENUES
}