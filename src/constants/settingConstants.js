export const SETTING_DATA = [
    {
        id: 1,
        icon: 'key',
        name: 'Account',
        description: 'Privacy, security, change number'
    },
    {
        id: 2,
        icon: 'comment',
        name: 'Chats',
        description: 'Backup, history, wallpaper'
    },
    {
        id: 3,
        icon: 'bell',
        name: 'Notification',
        description: 'Message, group & call tones'
    },
    {
        id: 4,
        icon: 'sync',
        name: 'Data and storage usage',
        description: 'Network usage, auto-download'
    },
    {
        id: 5,
        icon: 'question-circle',
        name: 'Help',
        description: 'FAQ, contact us, privacy policy'
    },
    {
        id: 6,
        icon: 'user-friends',
        name: 'Invite a friend',
        description: ''
    }
];