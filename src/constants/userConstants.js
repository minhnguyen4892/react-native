const USER_PROFILE = {
    urlImage: 'https://facebook.github.io/react/logo-og.png',
    userName: 'MINH NGUYEN',
    phone: '0367288217'
};

const USER_DATA = [
    {
        id: 1,
        leftIcon: 'user',
        name: 'Name',
        subtitle: 'Minh Nguyen',
        rightIcon: 'edit'
    },
    {
        id: 2,
        leftIcon: 'info-circle',
        name: 'About',
        subtitle: 'Hey there! I am using Example App',
        rightIcon: 'edit'
    },
    {
        id: 3,
        leftIcon: 'phone',
        name: 'Phone',
        subtitle: '036 728 8217'
    }
];

export {
    USER_PROFILE,
    USER_DATA
}