/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createStackNavigator, createAppContainer, createSwitchNavigator, createBottomTabNavigator } from "react-navigation";
import TodoComponent from './src/container/TodoContainer'
import LoginComponent from './src/container/LoginContainer'
import HomeComponent from './src/container/HomePageContainer';
import ProfileComponent from './src/container/ProfileContainer';
import {getTabBarIcon} from './src/components/TabBarIcon';
import DetailItemComponent from './src/components/ItemDetailComponent';
import SettingComponent from './src/container/SettingContainer';
import MapComponent from './src/container/MapContainer';
import {Provider} from 'react-redux';
import configureStore from "./src/store";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


const HomeStack = createStackNavigator({
    Home: HomeComponent,
    Todos: TodoComponent,
    DetailPage: DetailItemComponent
});

const SettingStack = createStackNavigator({
   SettingContainer: SettingComponent,
   Profile: ProfileComponent
});

const MainNavigation = createBottomTabNavigator({
    DashBoard: HomeStack,
    Map: MapComponent,
    Setting: SettingStack,
},{
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) =>
            getTabBarIcon(navigation, focused, tintColor),
    }),
    tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
    },
});
const MainContainer = createAppContainer(createSwitchNavigator({
    Main: MainNavigation,
    Login: LoginComponent
}
));

const store = configureStore();

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainContainer/>
            </Provider>
        )
    }
}
